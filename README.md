Ultrasonic Sensor Demo
======================
This is a simple program the works with HC-SR04 ultrasonic sensor. It makes a measurement with the sensor and reports the distance in inches via serial output. Measurements are made and reported every second.


## Hardware
This project uses an _Arduino Nano Atmega 328P_ board.

NOTE: If you use the Arduino IDE, you will need to manually select the target board.


## Building
This program requires the PlatformIO IDE. It will compile with the Arduino IDE, you will need to install the _NewPing_ library. Click [here](http://playground.arduino.cc/Code/NewPing) for instructions on how to install the library in the Arduino IDE.

### Commands

__Build__
```
pio run --target=upload
```

__Build and upload__
```
pio run --target=upload
```

__Monitor serial output__
```
pio device monitor -b9600
```

## Misc
__Example w/o NewPing library__
This is an example of the HC-SR04 ultrasonic sensor with the _NewPing_ library.
[HC-SR04 Ultrasonic range finder example](https://codebender.cc/sketch:356078#HC-SR04%20Ultrasonic%20Sensor%20Example.ino)
