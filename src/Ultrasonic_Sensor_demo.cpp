/*

    YourDuino SKETCH UltraSonic Serial 2.0
    Runs HC-04 and SRF-06 and other Ultrasonic Modules
    Open Serial Monitor to see results
    Reference: http://playground.arduino.cc/Code/NewPing

    Original author:  terry@yourduino.com
    Modified by: Chris Wilder (wilder.christopher@gmail.com)

 */
#include <NewPing.h>   //UltraSonic

/* ************************************************
   Macros
 * ************************************************/

/* ************************************************
   Constants
 * ************************************************/
//UltraSonic
#define  TRIGGER_PIN  7
#define  ECHO_PIN     8
#define MAX_DISTANCE 400 // Maximum distance we want to ping for (in centimeters).
                         //Maximum sensor distance is rated at 400-500cm.

/* ************************************************
  Global variabls
* ************************************************/
//UltraSonic
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
int distance_in;
int distance_cm;


/* ************************************************
  Function declaration
* ************************************************/

/* ************************************************
  Function definitions
* ************************************************/

/*
  Setup method.
*/
void setup() {
  Serial.begin(9600);
  Serial.println("Welcome to the Ultrasonic Sensor Demo");
}

/*
  Main program control loop
*/
void loop() {

  //Measure distance
  delay(1000);// Make one measurement per second. 29ms should be the shortest delay between pings.
  distance_in = sonar.ping_in();
  Serial.print("Distance: ");
  Serial.print(distance_in); // Convert ping time to distance and print result
                            // (0 = outside set distance range, no ping echo)
  Serial.println(" inches");
}
